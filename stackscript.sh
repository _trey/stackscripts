#!/bin/bash

# <UDF name="deploy_user" Label="Deployment User Account" default="deploy" example="User account to create for deployment and remote access." />
# <UDF name="deploy_user_password" Label="Deployment User Password" example="Password for the deployment user account" />
# <UDF name="deploy_user_pubkey" Label="Deployment User's SSH Public Key" default="" example="SSH public key to be associated with the Deploymeny User account" />
# <UDF name="host" Label="System Host Name" default="" example="The host name for this system" />
# <UDF name="domain" Label="System Domain Name" default="usaplus.com" example="The domain name for this system" />
# <UDF name="ruby_version" Label="Ruby Version to Install" default="2.1.5" example="The version of Ruby to be installed" />
# <UDF name="postgres_password" Label="Postgres Password" example="Password for the main postgres user." />

# StackScript written by Trey Stoffregen <trey@wadeh.com>

exec &> /root/stackscript.log

source <ssinclude StackScriptID="1">

function log {
  echo "$1 `date '+%D %T'`"
}

function lower {
    # helper function
    echo $1 | tr '[:upper:]' '[:lower:]'
}

function system_primary_ip6 {
  echo $(ifconfig eth0 | awk '/inet6 addr:/ {print substr($3, 0, index($3,"/")); exit}')
}

function system_sshd_edit_bool {
    # system_sshd_edit_bool (param_name, "Yes"|"No")
    VALUE=`lower $2`
    if [ "$VALUE" == "yes" ] || [ "$VALUE" == "no" ]; then
        sed -i "s/^#*\($1\).*/\1 $VALUE/" /etc/ssh/sshd_config
    fi
}

function system_sshd_permitrootlogin {
    system_sshd_edit_bool "PermitRootLogin" "$1"
}

function system_sshd_passwordauthentication {
    system_sshd_edit_bool "PasswordAuthentication" "$1"
}

function system_security_configure_ufw {
  ufw logging on
  ufw default deny
  ufw allow ssh/tcp
  ufw limit ssh/tcp
  ufw allow http/tcp
  ufw allow https/tcp
  ufw enable
}

function system_apt_add_backports {
  echo '' >> /etc/apt/sources.list
  echo '# wheezy-backports' >> /etc/apt/sources.list
  echo 'deb http://mirrors.linode.com/debian/ wheezy-backports main' >> /etc/apt/sources.list
  echo 'deb-src http://mirrors.linode.com/debian/ wheezy-backports main' >> /etc/apt/sources.list
  aptitude update
}

function system_rbenv_install {
  # system_rbenv_install (user_name, ruby_version)
  
  su -c 'git clone https://github.com/sstephenson/rbenv.git ~/.rbenv' - $1
  su -c 'touch ~/.profile' - $1
  echo '' >> /home/$1/.profile
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> /home/$1/.profile
  echo 'eval "$(rbenv init -)"' >> /home/$1/.profile
  su -c 'touch ~/.gemrc' - $1
  echo "gem: --no-rdoc --no-ri" >> /home/$1/.gemrc
  log "-----> Installed rbenv and set prefs"
  su -c 'git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build' - $1
  su -c 'git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash' - $1
  log "-----> Installed rbenv plugins ruby-build and rbenv-gem-rehash"
  su -c 'source ~/.profile' - $1
  su -c "rbenv install $2" - $1
  su -c "rbenv global $2" - $1
  log "-----> Installed ruby version $2"
}

# Variables
IP_ADDRESS=`system_primary_ip`
IP6_ADDRESS=`system_primary_ip6`

# Set host and domain
echo '' >> /etc/hosts
echo '# Local Environment' >> /etc/hosts
system_add_host_entry "$IP_ADDRESS" "$HOST.$DOMAIN $HOST"
system_add_host_entry "$IP6_ADDRESS" "$HOST.$DOMAIN $HOST"
system_set_hostname "$HOST"
log "-----> Set hostname to $HOST"

# Set timezone to US Central
echo "America/Chicago" > /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata
log "-----> Set timezone to America/Chicago"

# Update system software
system_update
log "-----> Updated system"

# Add deployment user and allow sudo without password
user_add_sudo "$DEPLOY_USER" "$DEPLOY_USER_PASSWORD"
user_add_pubkey "$DEPLOY_USER" "$DEPLOY_USER_PUBKEY"
echo "$DEPLOY_USER ALL=(ALL) NOPASSWD:ALL" > /tmp/$DEPLOY_USER.sudo
chown root:root /tmp/$DEPLOY_USER.sudo
chmod 0440 /tmp/$DEPLOY_USER.sudo
mv /tmp/$DEPLOY_USER.sudo /etc/sudoers.d/$DEPLOY_USER
log "-----> Added user $DEPLOY_USER with password $DEPLOY_USER_PASSWORD"

# Add www group to own web app files
usermod -a -G www-data $DEPLOY_USER
log "-----> Added deploument user to www-data group"

# Create /var/www folder
mkdir /var/www
chown -R www-data:www-data /var/www
chmod -R 770 /var/www
log "-----> Created /var/www"

# Configure SSHD
system_sshd_permitrootlogin "no"
system_sshd_passwordauthentication "no"
touch /tmp/restart-ssh
log "-----> Configured SSHD"

# Firewall
aptitude -y install ufw
system_security_configure_ufw
log "-----> Installed UFW and configured firewall"

# Fail2Ban
aptitude -y install fail2ban
log "-----> Installed fail2ban"

# Dev Tools
aptitude -y install curl build-essential zlib1g-dev libssl-dev libcurl4-openssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt1-dev
aptitude -y install git
log "-----> Installed ruby prereqs and git"

#Install Nginx
aptitude -y install nginx
log "-----> Installed Nginx"

# Rbenv installed under deployment user account
log "-----> Setting up rbenv under $DEPLOY_USER"
system_rbenv_install "$DEPLOY_USER" "$RUBY_VERSION"

# Install Node.js
system_apt_add_backports
log "-----> Added backports"
aptitude -y install nodejs
log "-----> Installed nodejs"

# Install database
aptitude -y install sqlite3
log "-----> Installed sqlite3"
aptitude -y install postgresql postgresql-client libpq-dev postgresql-contrib
su -c "psql -c \"CREATE EXTENSION adminpack;\"" - postgres
log "-----> Installed PostgreSQL and extension adminpack"

# Change postgres password
su -c "psql -c \"ALTER USER postgres WITH PASSWORD '$POSTGRES_PASSWORD';\"" - postgres
log "-----> Changed postgres password"

# Install bundler gem
su -c 'gem install bundler' - $DEPLOY_USER
log "-----> Installed bundler gem"

# Install good stuff
goodstuff

# Clean
aptitude clean

restartServices
log "-----> Restarted services"

log "-----> Finished System Setup"

# TODO Server setup
# - copy deploy user private and public keys to ~/.ssh/id_rsa and ~/.ssh/id_rsa.pub
# - copy deploy user public key to git repo so that server can access private git repos
# - upload nginx.conf
# - upload SSL certificate and private key to /etc/ssl/certs and /etc/ssl/private

# TODO Setup per app
# - Create DB user and DB
# - configuration files in /shared/config/
# - Capistrano/Nginx/Unicorn confing in actual project
