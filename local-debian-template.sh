# Update system (as root)
#(NOTE ... aptitude no longer included with Debian 9. Use plain apt instead?)
aptitude update
aptitude -y full-upgrage

apt install qemu-guest-agent

# Firewall
aptitude install ufw
ufw logging on
ufw default deny
ufw allow ssh/tcp
ufw limit ssh/tcp
ufw enable

aptitude install fail2ban

aptitude install ntp
# edit /etc/ntp.conf to point at local time server?

# edit /etc/default/ntp ... add -4 to NTPD command line options to disable ipv6
# Update restrict clauses in /etc/ntp.conf for local network?

# install sudo
aptitude install sudo
usermod -aG sudo localadmin #(or whatever SSH user)

# AD Integration
aptitude install realmd adcli sssd sssd-tools samba-common --> krb5-config
  --> policykit-1 packagekit # needed on Debian 9?
	---> cifs-utils keyutils # for krb5 mounting CIFS share
systemctl enable sssd #(sssd not enabled at boot by install)

##CREATE /etc/reamld.conf

--># [active-directory]
--># os-name = Debian
--># os-version = 8.5.0

# [users]
# default-home = /home/%u
# default-shell = /bin/bash

--># [internal.wadeh.com]
--># fully-qualified-names = no

# create /usr/share/pam-configs/mkhomedir with the following
Name: Create home directory during login
Default: yes
Priority: 900
Session-Type: Additional
Session:
        required        pam_mkhomedir.so umask=0022 skel=/etc/skel
        
# Then run pam-auth-update

echo "%domain\ admins ALL=(ALL) ALL" | sudo tee -a /etc/sudoers.d/wadeh

# On each host after clone
# set default_domain_suffix = internal.wadeh.com in /etc/sssd/sssd.conf
# set full_name_format = %1$s in /etc/sssd/sssd.conf
realm join --user=administrator INTERNAL.WADEH.COM
# set Fully qualified domain = False in appropriate domain section of /etc/sssd/sssd.conf
# Check /etc/hostname ... short name only
# Check /etc/hosts ... 127.0.0.1 second entry should be fully qualified name followed by short name
#
# remove firstboot stuff
# Remove S04-virt-sysprep-firstboot from /etc/init.d folders ... init.d, rc2.3, rc3.d, rc5.d
# Remove /etc/systemd/system/default.target.wants/firstboot.service
# Remove -rf /usr/lib/virt-sysprep

# END AD INTRGRATION

# Upload SSH public key to ~/.ssh/authorized_keys

# SSHD 
#permitrootlogin nr
#permitpasswordlogin no

# Then in host to sysprep
virt-sysprep --operations defaults,-ssh-userdir --firstboot-command 'dpkg-reconfigure openssh-server' -d debian-8.5.0-amd64-template

# Then for clones
virt-customize --hostname xxx --domain xxx
