#!/bin/bash

# INSTALL NOTE - upgrade to backports latest kernel for NVME M.2 drive support. See below
# NO LONGER NECESSARY SINCE KERNEL 4 BECAME STANDARD
# chroot into install
#cd /target
#mount -t proc proc proc/
#mount --rbind /sys sys/
#mount --rbind /dev dev/
#mount --rbind /run run/
#chroot . /bin/bash

# Get new kernel
#apt-get update
#apt-get -t jessie-backports install linux-image-amd64
#apt-get -t jessie-backports install grub-efi
#update-grub
#grub-install

# END INSTALL

# Enable IOMMU via boot param
# Edit /etc/defaults/grub
# Add intel_iommu=on to GRUB_CMDLINE_LINUX

# reboot

# Update system (as root)
aptitude update
aptitude -y full-upgrage

# install sudo
aptitude install sudo
usermod -aG sudo tstoffregen #(or whatever SSH user)


# Upload SSH public key to ~/.ssh/authorized_keys

# SSHD 
#permitrootlogin no
#l no

# Remove old kernel that was replaced during install
aptitude purge linux-image-x.x.x-x-amd64

#Install KVM stuff
#aptitude install qemu-kvm libvirt-bin libguestfs-tools
apt install --no-install-recommends qemu-kvm libvirt-clients libvirt-daemon-system
adduser tstoffregen kvms
adduser tstoffregen libvirt
aptitude install libguestfs-tools # for virt-sysprep

# For remote access to libvirt
# update /etc/libvirt/libvirtd.conf:
#    unix_sock_grooup = "libvirt"
#    unix_sock_rw_perms = "0770"

# Prepare LSI HBA for PCI Passthrough
echo '0000:05:00.0' | sudo tee /sys/bus/pci/devices/0000\:05\:00.0/driver/unbind
sudo modprobe vfio
sudo modprobe vfio_pci
echo 1000 0087 | sudo tee /sys/bus/pci/drivers/vfio-pci/new_id

# Install Open vSwitch
aptitude install openvswitch-switch openvswitch-common

# Configure OVS Bridge
ovs-vsctl add-br br0 # (or whatever name is appropriate)

# Update /etc/network/interfaces (see accompanying config example)

# Configure libvirt networking to use OVS
virsh net-define network-ovs.xml
virsh net-start ovs
virsh net-autostart ovs

# NTP
aptitude install ntp

# Firewall
aptitude install ufw
ufw logging on
ufw default deny
ufw allow ssh/tcp
ufw limit ssh/tcp
ufw allow http/tcp
ufw allow https/tcp
ufw enable

aptitude install fail2ban

# CREATED CLONEZILLA BACKUP HERE
