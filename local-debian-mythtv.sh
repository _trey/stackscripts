#!/bin/bash

# Update system (as root)
aptitude update
aptitude -y full-upgrade

#TODO ... this shoudl be added "contrib non-free" after existing jessie main sources.
# realtek firmware (for zotac box)
echo '' >> /etc/apt/sources.list
echo '# jessie non-free' >> /etc/apt/sources.list
echo 'deb http://http.debian.net/debian/ jessie/main/contrib non-free' >> /etc/apt/sources.list
aptitude update
aptitude -y install firmware-realtek

# Nvidia drivers DONT DO THIS? DIDN't SO FAR
apt-get install nvidia-kernel-2.6-686 # NOT right version though
apt-get install nvidia-glx 
apt-get nvidia-xconfig mesa-utils #maybe not?
nvidia-xconfig -o /etc/X11/xorg.conf #maybe not?

# install sudo
aptitude install sudo
usermod -aG sudo tstoffregen #(or whatever SSH user)

# Upload SSH public key to ~/.ssh/authorized_keys

# SSHD 
#permitrootlogin no
#permitpassword no

# Firewall
aptitude -y install ufw
#configure script like the other + allow myth backend default ports
ufw allow 6543/tcp
ufw allow 6544/tcp
ufw allow in proto udp to 192.168.1.0/24
ufw allow in proto udp from 192.168.1.0/24

# Fail2ban
aptitude -y install fail2ban

# MySQL
# Install MySQL
mysql_install $MYSQL_PASSWORD #from stackscripts to set PWD
#mysql_tune "25" #Needed?
log "-----> Installed MySQL Server"

##TODO mysql_secure_installation?

##TODO SET variables for Myth
#password for MySQL root user (set previously)

# MythTV Backend w/ Database
echo '' >> /etc/apt/sources.list
echo '# deb-multimedia repositiry' >> /etc/apt/sources.list
echo 'deb http://www.deb-multimedia.org/ jessie main non-free' >> /etc/apt/sources.list
echo 'deb http://www.deb-multimedia.org/ jessie-backports main' >> /etc/apt/sources.list
aptitude update
aptitude -y install deb-multimedia-keyring
aptitude update
aptitude -y install mythtv-backend

# HDHomeRun
aptitude -y install hdhomerun-config
hdhomerun_config discover

# Create dirs for MythTV data
mkdir /var/lib/mythtvdata
chown mythtv:mythtv /var/lib/mythtv/data
chmod 775 /var/lib/mythtv/data

# Mythweb (including apache and PHP)
sudo aptitude -y install mythweb

#Created image at this point

#TODO ... configure apache accordingly root as mythtv, etc.