# Install MySQL
mysql_install $POSTGRES_PASSWORD
mysql_tune "25"
log "-----> Installed MySQL Server"

##TODO mysql_secure_installation?

# Install PHP-FPM
aptitude -y install php5-fpm php5-cli php5-mcrypt php5-mysql
log "-----> Installed PHP5 FPM"

#TODO nginx conf? or leave this to cap?